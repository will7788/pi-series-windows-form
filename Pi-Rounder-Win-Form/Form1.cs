﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pi_Rounder_Win_Form
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            double output = 4.0;
            try
            {
                int input = Int32.Parse(userInput.Text);
                if (input > 0)
                {
                    for (int i = 0; i < input; i++)
                    {
                        if (i % 2 == 0 && i >= 1)
                        {
                            output += (4.0 / ((2 * i) + 1));
                        }
                        else if (i % 2 == 1 && i >= 1)
                        {
                            output -= (4.0 / ((2 * i) + 1));
                        }
                    }
                    resultLabel.Text = "Approx Value of pi after " + input + " terms: " + output;
                }
                else
                {
                    MessageBox.Show("Enter a valid integer greater than zero.");
                }
            }
            catch(Exception ex) {MessageBox.Show(ex.Message);}
        }
    }
}
